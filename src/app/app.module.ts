import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductsComponent } from './layouts/products/product.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ManagementComponent, ConfirmDialogComponent } from './layouts/management/management.component';
import { UpdateProductComponent } from './layouts/update-product/update-product.component';
import { DetailProductComponent } from './layouts/detail-product/detail-product.component';
import { ImportImageComponent } from './component/import-image/import-image.component';
import { DemoMaterialModule } from './material-module';
import { SlideshowModule } from 'ng-simple-slideshow';
import { ToastrModule } from 'ngx-toastr';


@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    UpdateProductComponent,
    ConfirmDialogComponent,
    DetailProductComponent,
    ManagementComponent,
    ImportImageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    DemoMaterialModule,
    SlideshowModule,
    CommonModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      preventDuplicates: true,
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
