import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';


import { Observable } from 'rxjs';

import { Product, Type, Category } from '../layouts/product';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  typesUrl = environment.API_URL + 'get-type';
  categoriesUrl = environment.API_URL + 'get-cate';
  searchUrl = environment.API_URL + 'search';
  addProductUrl = environment.API_URL + 'add-product';
  getProductUrl = environment.API_URL + 'get-by-id';
  deleteProductUrl = environment.API_URL + 'delete-product';
  updateProductUrl = environment.API_URL + 'update-product';

  constructor(private http: HttpClient) {}

  addProduct(data: any): Observable<[]> {
    return this.http.post<any>(this.addProductUrl, data);
  }

  updateProduct(data: any): Observable<[]> {
    return this.http.put<any>(this.updateProductUrl, data);
  }

  getTypes(): Observable<Type[]> {
    return this.http.get<Type[]>(this.typesUrl);
  }

  getCategories(type: string): Observable<string> {
    return this.http.get<string>(this.categoriesUrl + '/' + type);
  }

  getProduct(id: string): Observable<Product> {
    return this.http.get<Product>(this.getProductUrl + '/' + id);
  }

  deleteProduct(id: string): Observable<any> {
    return this.http.delete(this.deleteProductUrl  + '/' + id);
  }

  getAccordSearch(data: any): Observable<Product[]> {
    return this.http.get<Product[]>(this.searchUrl
      + '?type=' + data.type
      + '&category=' + data.category
      + '&index=' + data.pageIndex
      + '&size=' + data.pageSize
      + '&keyword=' + data.keyword
      );
  }
}
