import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductsComponent } from './layouts/products/product.component';
import { ManagementComponent } from './layouts/management/management.component';
import { UpdateProductComponent } from './layouts/update-product/update-product.component';
import { DetailProductComponent } from './layouts/detail-product/detail-product.component';
const routes: Routes = [
  {
    path: 'list-product',
    children: [
      {
        path: 'grid',
        component: ProductsComponent
      },
      {
        path: 'detail-product/:id',
        component: DetailProductComponent
      },
      { path: '**', redirectTo: 'grid' }
    ]
  },
  {
    path: 'product-management',
    children: [
      {
        path: 'list-product-table',
        component: ManagementComponent
      },
      {
        path: 'update-product/:id',
        component: UpdateProductComponent
      },
      { path: '**', redirectTo: 'list-product-table' }
  ]
  },
  { path: '**', redirectTo: 'list-product' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
