export interface Product {
  _id: string;
  product_code: string;
  name: string;
  type: string;
  category: string;
  description: string;
  price: number;
  img_url: any[];
}
export interface Type {
  id: string;
  value: string;
}
export interface Category {
  id: string;
  value: string;
}
