import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { Product, Type } from '../product';
import { ProductsService } from '../../services/product.service';
import {FormControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-confirm-dialog',
  templateUrl: 'confirm-dialog.component.html',
})
export class ConfirmDialogComponent {}

@Component({
  selector: 'app-management-product',
  templateUrl: 'management.component.html',
  styleUrls: ['management.component.scss'],
})
export class ManagementComponent implements AfterViewInit, OnInit {

  addProductForm: FormGroup;
  file: any;
  products: Product[] = [];
  totalProduct: number;
  types: Type[] = [];
  categories: [] = [];
  imageSrc: string;
  chkImg = false;
  DataNumberPage = {pageIndex: 0, pageSize: 10};
  displayedColumns: string[] = ['index', 'name', 'product_code', 'category', 'price', 'action'];
  dataSource = new MatTableDataSource<Product>(this.products);

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  get f(): any {
    return this.addProductForm.controls;
  }
  changeFile(e): void {
    this.file = e;
  }
  changeChkImg(e): void {
    this.chkImg = e;
  }
  // get fileForm() {
  //   return this.addProductForm.get('file') as FormGroup;
  // }

  constructor(
    private formBuilder: FormBuilder,
    private productsService: ProductsService,
    private toastr: ToastrService,
    public dialog: MatDialog) {
    this.addProductForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      price: ['', [Validators.required]],
      type: ['', [Validators.required]],
      category: ['', [Validators.required]],
      description: ['', [Validators.required]],
      file: [null, [Validators.required]]
    });
  }
  ngOnInit(): void {
    this.getProductsSearch();
    this.getTypes();
  }
  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }
  getProductsSearch(method?: string): void {
    const dataEmit = {
      type: '',
      category: '',
      keyword: '',
      pageIndex: this.DataNumberPage.pageIndex,
      pageSize: this.DataNumberPage.pageSize
    };
    this.productsService.getAccordSearch(dataEmit)
    .subscribe((products: any) => {
      this.products = products.data;
      this.totalProduct = products.total;
      if (method === 'search') {
        if (!products.success) { this.toastr.error(products.message); }
      }
      this.dataSource = new MatTableDataSource<Product>(this.products);
    });
  }
  getPage(event: any): void {
    this.DataNumberPage.pageIndex = event.pageIndex;
    this.DataNumberPage.pageSize = event.pageSize;
    this.getProductsSearch();
  }
  typeSelected(): void {
    this.addProductForm.controls.category.setValue('');
    this.getCategories(this.addProductForm.value.type);
  }
  getTypes(): void {
    this.productsService.getTypes()
      .subscribe((types: any) => {
        this.types = types.data;
      });
  }
  getCategories(data: any): void {
    this.productsService.getCategories(data)
    .subscribe((categories: any) => {
      this.categories = categories.data;
    });
  }
  // inputValidator(event: any) {
  //   const pattern = /^[0-9]*$/;
  //   if (!pattern.test(event.target.value)) {
  //     event.target.value = event.target.value.replace(/[^0-9]/g, "");
  //   }
  // }
  submit(formDirective): void{
    this.addProductForm.controls.name.setValue(this.addProductForm.value.name.trim());
    this.addProductForm.controls.description.setValue(this.addProductForm.value.description.trim());
    if (!this.file) {
      this.chkImg = true;
    } else {
      if (this.addProductForm.controls.file.value) {
        if (!this.addProductForm.invalid) {
          const formData = new FormData();
          formData.append('name', this.addProductForm.value.name);
          formData.append('type', this.addProductForm.value.type);
          formData.append('category', this.addProductForm.value.category);
          formData.append('description', this.addProductForm.value.description);
          formData.append('price', this.addProductForm.value.price);
          formData.append('file', this.file);
          this.productsService.addProduct(formData)
          .subscribe((res: any) => {
            this.DataNumberPage.pageIndex = 0;
            this.getProductsSearch();
            this.toastr.success(res.message);
          }, err => {
            this.toastr.error(err.error.message);
          });
          formDirective.resetForm();
          this.addProductForm.reset();
          this.imageSrc = null;
        }
      }
    }
  }
  openDialog(id: string): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        this.productsService.deleteProduct(id)
          .subscribe((res: any) => {
            this.DataNumberPage.pageIndex = 0;
            this.toastr.info(res.message);
            this.getProductsSearch();
            this.getTypes();
            this.addProductForm.controls.category.setValue('');
          }, err => {
            this.toastr.error(err.error.message);
          });
      }
    });
  }
}
