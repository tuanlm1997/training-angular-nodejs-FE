import { Product } from '../product';
import {Component, OnInit} from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ProductsService } from '../../services/product.service';

@Component({
  selector: 'app-detail-product',
  templateUrl: 'detail-product.component.html',
  styleUrls: ['detail-product.component.scss'],
})
export class DetailProductComponent implements OnInit {
  constructor(private route: ActivatedRoute, private productsService: ProductsService) { }
  id: string;
  product: Product;
  slides: any[] = [];
  price: string;
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
    });
    this.getProductById(this.id);
    console.log(this.price);
  }

  getProductById(id: string): void {
    this.productsService.getProduct(id)
    .subscribe((product: any) => {
      this.product = product.data;
      this.slides = this.product.img_url.map((el, index) => {
        return {
          url: el.url,
        };
      });
      this.price = new Intl.NumberFormat(['ban', 'id']).format(this.product.price);
    });
  }
}
