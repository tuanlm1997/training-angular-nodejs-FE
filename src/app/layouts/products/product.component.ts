import { Component, OnInit } from '@angular/core';

import { Product, Type } from '../product';
import { ProductsService } from '../../services/product.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-products',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})

export class ProductsComponent implements OnInit {
  constructor(private productsService: ProductsService, private toastr: ToastrService) {}
  selectedType: string;
  selectedCate: string;
  itemImageUrl: string;
  products: Product[];
  totalProduct: number;
  types: Type[];
  categories: string[];
  searchValue = '';
  DataNumberPage = {pageIndex: 0, pageSize: 8};

  ngOnInit(): void {
    this.selectedType = '';
    this.selectedCate = '';
    this.getProductsSearch();
    this.getTypes();
  }
  getProducts(data: any): void {
    this.productsService.getAccordSearch(data)
      .subscribe((products: any) => {
        this.products = products.data;
        this.totalProduct = products.total;
      });
  }
  getTypes(): void {
    this.productsService.getTypes()
      .subscribe((types: any) => {
        this.types = types.data;
        this.types.unshift({id: 'Select All', value: ''});
      });
  }
  getCategories(type: string): void {
    this.productsService.getCategories(type)
    .subscribe((categories: any) => {
      this.categories = categories.data;
      this.categories.unshift('');
    });
  }
  getProductsSearch(method?: string): void {
    const dataEmit = {
      type: this.selectedType,
      category: this.selectedCate,
      keyword: this.searchValue,
      pageIndex: this.DataNumberPage.pageIndex,
      pageSize: this.DataNumberPage.pageSize
    };
    this.productsService.getAccordSearch(dataEmit)
    .subscribe((products: any) => {
      this.products = products.data;
      this.totalProduct = products.total;
      if (method === 'search') {
        if (!products.success) { this.toastr.error(products.message); }
      }
    });
  }
  onKey(event): void {
    if (event.key === 'Enter') {
      if (!this.searchValue) {
        this.toastr.error('Please input search value!');
      } else {
        this.getProductsSearch('search');
      }
    }
  }
  cateSelected(): void {
    this.searchValue = '';
    this.DataNumberPage.pageIndex = 0;
    this.getProductsSearch();
  }
  typeSelected(): void {
    this.selectedCate = '';
    this.searchValue = '';
    this.DataNumberPage.pageIndex = 0;
    if (this.selectedType === '') {
      this.getProductsSearch();
      this.categories = [];
    } else {
      this.getCategories(this.selectedType);
      this.getProductsSearch();
    }
  }
  getPage(event: any): void {
    this.DataNumberPage.pageIndex = event.pageIndex;
    this.DataNumberPage.pageSize = event.pageSize;
    this.getProductsSearch();
  }
  getSearch(): void {
    this.DataNumberPage.pageIndex = 0;
    if (!this.searchValue) {
      this.toastr.error('Please input search value!');
    } else {
      this.getProductsSearch('search');
    }
  }
}
