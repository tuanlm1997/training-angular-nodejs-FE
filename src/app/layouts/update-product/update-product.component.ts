import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Product, Type } from '../product';
import { ProductsService } from '../../services/product.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-product',
  templateUrl: 'update-product.component.html',
  styleUrls: ['update-product.component.scss'],
})
export class UpdateProductComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private productsService: ProductsService,
    private router: Router,
    private toastr: ToastrService,
    ) {
    this.updateProductForm = this.formBuilder.group({
      name: new FormControl('', [Validators.required, Validators.minLength(3)]),
      price: new FormControl('', [Validators.required]),
      type: new FormControl('', [Validators.required]),
      category: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      file0: new FormControl(null),
      file1: new FormControl(null),
      file2: new FormControl(null),
      file3: new FormControl(null),
      file4: new FormControl(null),
    });
  }
  id: string;
  product: Product;
  updateProductForm: FormGroup;
  types: Type[];
  files: [
    any, any, any, any, any
  ];
  imgSrc: [
    string, string, string, string, string
  ];
  categories: [] = [];
  get f(): any {
    return this.updateProductForm.controls;
  }
  changeFile(e, index): void {
    this.files[index] = e;
  }

  ngOnInit(): void {
    // console.log('thing ==>', this.route.snapshot.params.id);
    this.files = [ null, null, null, null, null ],
    this.imgSrc = [
      '', '', '', '', ''
    ],
    this.route.paramMap.subscribe(params => {
      // console.log(params.get('id'));
      this.id = params.get('id');
    });
    this.getProductById(this.id);
    this.getTypes();
  }

  getProductById(id: string): void {
    this.productsService.getProduct(id)
    .subscribe((product: any) => {
      this.product = product.data;
      this.getCategories(this.product.type);
      this.updateProductForm.controls.name.setValue(this.product.name);
      this.updateProductForm.controls.type.setValue(this.product.type);
      this.updateProductForm.controls.price.setValue(this.product.price);
      this.updateProductForm.controls.category.setValue(this.product.category);
      this.updateProductForm.controls.description.setValue(this.product.description);
      this.imgSrc[0] = this.product.img_url[0].url;
      for (const srcProduct of this.product.img_url) {
        this.imgSrc[srcProduct.index] = srcProduct.url;
      }
    });
  }
  getTypes(): void {
    this.productsService.getTypes()
      .subscribe((types: any) => {
        this.types = types.data;
      });
  }
  typeSelected(): void {
    this.updateProductForm.controls.category.setValue('');
    this.getCategories(this.updateProductForm.value.type);
  }
  getCategories(type: string): void {
    this.productsService.getCategories(type)
    .subscribe((categories: any) => {
      this.categories = categories.data;
    });
  }
  submit(): void{
    this.updateProductForm.controls.name.setValue(this.updateProductForm.value.name.trim());
    this.updateProductForm.controls.description.setValue(this.updateProductForm.value.description.trim());
    if (this.updateProductForm.valid) {
      const formData = new FormData();
      formData.append('id', this.id);
      formData.append('name', this.updateProductForm.value.name);
      formData.append('type', this.updateProductForm.value.type);
      formData.append('category', this.updateProductForm.value.category);
      formData.append('description', this.updateProductForm.value.description);
      formData.append('price', this.updateProductForm.value.price);
      const arrayIndex: number[] = [];
      this.files.forEach((file, index) => {
        if (file) {
          formData.append('file', file);
          arrayIndex.push(index);
        }
      });
      const indexDeleteArray: number[] = [];
      for (const [index, src] of this.imgSrc.entries()) {
        if (index) {
          if (!src) {
            indexDeleteArray.push(index);
          }
        }
      }
      formData.append('index', JSON.stringify(arrayIndex));
      formData.append('indexDelete', JSON.stringify(indexDeleteArray));
      this.productsService.updateProduct(formData)
      .subscribe((res: any) => {
        this.router.navigate(['../'], {
          relativeTo: this.route,
        });
        this.toastr.success(res.message);
      }, err => {
        this.toastr.error(err.error.message);
      });
    }
  }
}
