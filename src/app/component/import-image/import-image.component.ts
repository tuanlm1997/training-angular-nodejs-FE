import { Component, Input, Output, EventEmitter } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-import-image',
  templateUrl: 'import-image.component.html',
  styleUrls: ['import-image.component.scss'],
})
export class ImportImageComponent {
  @Input() imageSrc: any;
  @Input() file: any;
  @Input() control: AbstractControl;
  @Input() chkImg?: boolean;
  @Input() canDelete: boolean;
  @Output() changeChkImg = new EventEmitter();
  @Output() fileChange = new EventEmitter();
  @Output() imageSrcChange = new EventEmitter();
  showImg: boolean;
  constructor(private toastr: ToastrService) { }

  onFileChange(event: any): void {
    if (event.target.files[0].type.split('/')[0] === 'image') {
      const reader = new FileReader();
      if (event.target.files && event.target.files.length) {
        const [file] = event.target.files;
        this.fileChange.emit(file);
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.imageSrc = reader.result as string;
          this.imageSrcChange.emit(this.imageSrc);
          // this.addProductForm.patchValue({
          //   fileSource: reader.result
          // });
        };
        if (this.chkImg) {
          this.changeChkImg.emit(false);
        }
      }
    } else {
      this.toastr.error('You need import file imgae');
    }
}
  removeImg(): void {
    this.imageSrc = '';
    this.file = null;
    this.imageSrcChange.emit(this.imageSrc);
    this.fileChange.emit(this.file);
  }
  viewImg(): void{
    this.showImg = true;
  }
  offViewImg(): void {
    this.showImg = false;
  }
}
